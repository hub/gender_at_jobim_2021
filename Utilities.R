library(data.table)
library(stringi)
library(stringr)

clean_first_name <- function(x){as.character(stri_trans_general(str_trim(tolower(x)), id="Latin-ASCII")) }

# color palette


gender_palette = c("#251351","#E77464", "#96CF68",  "#E89268", "#F1DD9A")
junior_contract= c('CDD/Post-Doc/PhD Student/Trainees', "Short term/Post-Doc/PhD Student/Trainees","unemployed", "Internship", "Student - Stagiaire" , "Stage", "TRAINEE", "apprenticeship", "IGR CDD", "apprentice")
senior_contract =  c("Permanent position in academia", "fonctionnaire", "CDI")

fr_names = fread("./data/French_name.csv")
session_meta = fread("./data/meta_data/dict_session.csv")
setkey(session_meta, "Session")

length(fr_names$`01_prenom`)
length(clean_first_name(fr_names$`01_prenom`))

fr_names[,`01_prenom`:=clean_first_name(`01_prenom`)]

recoded_gender = ifelse(fr_names[,`02_genre`] %in% c('m', 'f'), fr_names[,`02_genre`], NA)
fr_names[, Gender := ifelse(recoded_gender=="f",'Female', "Male")]

fr_names = rbind(fr_names, setNames(data.frame(prenom=c('anne-elodie', 'laetita','marie-france',"naoual",'raissa',"sidwell",'matéo','manuel guillermo', "geert-jan", "jaime", "omran", "vuong"), `02_genre`=rep(NA,12) ,Gender=c("Female", "Female", "Female", 'Female','Female','Female','Male', "Male", "Male","Male", "Male","Male")), names(fr_names)))
data.frame(prenom=c('anne-elodie', 'laetita','marie-france',"naoual",'raissa',"sidwell",'matéo','manuel guillermo', "geert-jan", "jaime", "omran", "vuong"), `02_genre`=rep(NA,12) ,Gender=c("Female", "Female", "Female", 'Female','Female','Female','Male', "Male", "Male","Male", "Male","Male"))

setkey(fr_names, `01_prenom`)
dim(fr_names)
fr_names= unique(fr_names[,.(`01_prenom`, Gender)])
dim(fr_names)
unique_names = as.character(fr_names[, .N, by=`01_prenom`][N==1,`01_prenom` ])
fr_names=fr_names[unique_names,]
dim(fr_names)
fr_names[,`01_prenom`:=as.character(`01_prenom`)]
fr_names[, Gender := as.character(Gender)]
setkey(fr_names, `01_prenom`)
