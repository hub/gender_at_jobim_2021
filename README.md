# JOBIM 2021 Gender analysis

This repository contains data and script related to our observation study of gender impact on asking behavior during JOBIM 2021. In agreement with our [data policy and RGPD regulations](https://research.pasteur.fr/en/project/jobim-2021-pilot-project-gender-speaking-differences-in-academia/), only aggregated, anonymous and/or publicly available information are posted in this repository. Zoom exports, registration survey and observation files containing names of askers and their accompanying scripts remains private.

## Citation

If you wish to use data in this repository please cite our [preprint](https://www.biorxiv.org/content/10.1101/2022.03.07.483337v1).

## ./data

* ./data/French_name.csv : database of common French firstnames and their usual gender.  

* ./data/Committee_composition
names of Committee members for each year where the information was publicly available from JOBIM websites.

* ./data/Results/Composition_committee.csv

Number of Committee members and fraction of women by years

* ./data/data_speaker/Speaker_parity.csv
Number of Speaker by year and their repartition by gender

* ./data/data_speaker/Keynote_parity.csv
Number of keynote speaker by year and their repartition by gender

* ./data/Zoom_export/processed_extract/Written_question_by_session.csv

Number of questions asked by sessions and there repartition by gender

* ./data/observation/observation_question_count.csv

Number of questions asked at the end of each talk
NWA : number of women in attendance
NMA : number of men in attendance

## ./src

This folder contains script used to process and analyse data

* Parity_in_committee.py : input : Committee members table by years /data
output : table of the fraction of women in each Committee (organization and program by years)

Process Committee members firstnames to count the Number of member by gender

* Demographics_evolution.R : draw the proportion of women in attendance, speakers and Committee (panel C, D, E and F of the paper figure 1 )

* written_questions_by_sessions.R : Script to draw figure 2.A of the paper
