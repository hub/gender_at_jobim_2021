import pandas as pd
from nltk.corpus import names
import unicodedata
import re
from statsmodels.stats.proportion import proportions_ztest
from Gender_function import remove_accent, get_first_name, Assign_gender

Count_data = pd.DataFrame(index=range(2015, 2022), columns = ["W_orga", "M_orga", "W_program", "M_program", "PW_orga", "PW_program", "Parity_orga", "Parity_program", "PdifferenceC"])

for y in range(2015, 2022):

    fin = "/home/hjulienn/Project/Gender_at_Pasteur/data/Committee_composition/"+str(y)+".csv"

    Compo = pd.read_csv(fin, sep="\t")
    Compo.columns = ["Orga", "Program"]

    C_orga = Compo.Orga
    C_orga =C_orga[~C_orga.isnull()]
    C_orga = C_orga.apply(get_first_name)
    G_orga = C_orga.apply(Assign_gender)
    print(C_orga[G_orga=="Androgynous"])
    print(Compo.loc[C_orga[G_orga=="Androgynous"].index, "Orga"])

    WO = (G_orga =="Woman").sum()
    MO = (G_orga =="Man").sum()
    NO = WO+MO
    Count_data.loc[y,"PW_orga"] = WO / (NO)
    Count_data.loc[y,"W_orga"] = WO
    Count_data.loc[y,"M_orga"] = MO

    stat, pval = proportions_ztest(WO, NO, value=0.5)
    Count_data.loc[y,"Parity_orga"] = pval

    C_program = Compo.Program
    C_program =C_program[~C_program.isnull()]
    C_program = C_program.apply(get_first_name)
    G_program = C_program.apply(Assign_gender)
    print(C_program[G_program=="Androgynous"])
    print(Compo.loc[C_program[G_program=="Androgynous"].index, "Program"])
    WP = (G_program =="Woman").sum()
    MP = (G_program =="Man").sum()

    # deals with ambiguous first names
    if ("Dominique TESSIER" in Compo.Program):
        WP= WP+1
    if ("Dominique JOLY" in Compo.Program):
        WP= WP+1
    if ("Morgan Magnin" in Compo.Program):
        MP = MP+1
    NP = WP+MP
    Count_data.loc[y,"W_program"] = WP
    Count_data.loc[y,"M_program"] = MP
    Count_data.loc[y,"PW_program"] = WP / (NP)
    stat, pval = proportions_ztest(WP, NP, value=0.5)
    Count_data.loc[y,"Parity_program"] = pval

    stat, pval = proportions_ztest([WO, NO], [WP, NP], value=0)
    Count_data.loc[y,"PdifferenceC"] = pval

    print("Dominique TESSIER" in Compo.Orga)
Count_data.to_csv("./data/Results/Composition_committee.csv", sep="\t")
